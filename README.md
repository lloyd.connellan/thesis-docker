# dockerproject

A template for setting up docker images for dune-python.
The idea is to copy all files into your own git repository and adapt
accordingly.

__Note__: that the Docker container for `example' is not uploaded.


# How to Build/Update/Run the Docker Image

To update the Docker image on `registry.dune-project.org`,
please follow these steps:

__Note__: you will have to change the
`andreas.dedner/example` to your own local space and desired container
name.

1. Pull the Docker base image:
    ```
    docker pull registry.dune-project.org/staging/dune-python:2.6
    ```
    Please do not omit this step just because you already have the image. Otherwise
    an outdated version of the base image might be used.

1. Actually build the Docker image:
    ```
    docker build . --no-cache -t registry.dune-project.org/andreas.dedner/example:latest
    ```
    Please do not omit `--no-cache` to ensure every step of the build is actually
    executed.

    *Note*: This will set up the Debian base system of the Docker image, which takes
    quite some time.

1. Verify your build by testing the Docker image locally:
    ```
    docker run --rm -v dune:/dune -p 127.0.0.1:8888:8888 registry.dune-project.org/andreas.dedner/example:latest
    ```
    Use your favorite web browser, visit http://127.0.0.1:8888, enter the password
    `dune`, and run the demo notebooks (depending on the jupyter version you
    might be given  tokenized http address to use instead).
    *Note*: If you already have a volume `dune`, you will not receive the updated demo notebooks. It might be better to start with a clean volume.

1. Log into the Docker registry:
    ```
    docker login registry.dune-project.org
    ```

1. Push the Docker image:
    ```
    docker push registry.dune-project.org/andreas.dedner/example:latest
    ```
    This step actually updates the Docker image on the server.

    **Warning:** Updating the official Docker image affects all users of the
    image once they pull it.
    Make sure you don't push a buggy combination of the DUNE modules.

1. Log out of the Docker registry:
    ```
    docker logout registry.dune-project.org
    ```

That's it.
