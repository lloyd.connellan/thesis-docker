
# coding: utf-8

# Preamble
# ========

# In[ ]:


import math

from ufl import *
from dune.ufl import Space, NamedConstant

import dune.fem as fem
import dune.create as create
from dune.generator import algorithm
from dune.common import FieldVector
from dune.grid import cartesianDomain, Marker, gridFunction
from dune.fem.function import levelFunction, integrate
from dune.plotting import plotPointData as plot
