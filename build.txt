to build (can use --no-cache if necessary):

docker build --rm . -t thesis
docker tag thesis lloydconnellan/thesis
docker login
docker push lloydconnellan/thesis

to run:

docker pull lloydconnellan/thesis
docker run --rm -v dune:/dune -p 127.0.0.1:8888:8888 lloydconnellan/thesis
