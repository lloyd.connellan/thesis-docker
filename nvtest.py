
# coding: utf-8

# ## Nonvariational Test Program

# In[ ]:


from dune.grid import cartesianDomain
from math import sqrt
from norms import compute_norms
from numpy import dot
import dune.create as create
import os.path,sys
import pickle
import time


# We start off by selecting the problem and method used, as well as the solution type and polynomial order. These options can be exchanged with the commented out options if desired.

# In[ ]:


problemName = 'laplace' # advection2, nonD
schemeName = 'nvdg'     # h1H, h1D2, l2H, l2D2, mu, feng, var
solutionName = 0        # 0=smooth, 1=rough, 2=exp
polOrder = 2            # or 1, 3


# We set up a simple Cartesian grid with default storage and solution `uh`.

# In[ ]:


grid = create.grid("ALUSimplex", cartesianDomain([0, 0], [1, 1], [4, 4]))
space = create.space("Lagrange", grid, dimrange=1, order=polOrder, storage='fem')
uh = space.interpolate([0], name="solution")


# We define $A$ using the problem data, and obtain the exact solution and boundary conditions.

# In[ ]:


from problems import problemDict, solutionList
A = problemDict[problemName]()
exact, dirichletBC = solutionList[solutionName]()


# The penalty $\beta$ is set to different values depending on the problem, as per the analysis.

# In[ ]:


if problemName == "advection2":
    max_eval = 4
elif problemName == "nonD":
    max_eval = 2
else:
    max_eval = 1
beta = polOrder*(polOrder + 1)*max_eval


# We also create the scheme using the schemeName above.

# In[ ]:


from schemes import schemeDict, solve
scheme = schemeDict[schemeName](A, exact, dirichletBC, space, beta=beta)


# Now for the solve step. We use the solve method found in schemes.py to solve the problem, and calculate the error. We store the errors using pickle, then refine the grid at each step, calculating the EOC.

# In[ ]:


for eoc_loop in range(5):
    start_time = time.time()
    print("#### START")
    uh.clear()
    uh, info, solver = solve( scheme, uh, A, exact, dirichletBC, version=schemeName, beta=beta)
    print("#### END", info)
    total_time = time.time() - start_time
    errors = compute_norms(grid, uh - exact, A)
    if eoc_loop == 0:
        if polOrder == 2:
            file_path = 'pickle/' + problemName + '_' + schemeName + '_' + solver + '.p'
        else:
            file_path = 'pickle' + str(polOrder) + '/' + problemName + '_' + schemeName + '_' + solver + '.p'
        file = open(file_path, 'wb')
    pickle.dump([grid.size(0), errors, info['linear_iterations'], total_time], file)
    grid.hierarchicalGrid.globalRefine(1)
file.close()


# For the final step we write the results to `results/pickled_results` and `results/eocs`, and plot the data in `plots/`.

# In[ ]:


import results
if polOrder == 2:
    results.wp()
else:
    results.wp(polOrder=str(polOrder))

