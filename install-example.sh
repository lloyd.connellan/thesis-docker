#!/bin/bash

set -e

mkdir /tmp/dune
cd /tmp/dune

export CMAKE_FLAGS=" \
  -DCMAKE_BUILD_TYPE=Release \
  -DBUILD_SHARED_LIBS=TRUE \
  -DDUNE_PYTHON_INSTALL_LOCATION=system \
"
wget -qO - https://gitlab.dune-project.org/dune-fem/dune-fem/repository/archive.tar.gz?ref=releases/2.6 | tar xz
wget -qO - https://gitlab.dune-project.org/dune-fem/dune-fempy/repository/archive.tar.gz?ref=master | tar xz

dunecontrol all
dunecontrol make install

cd /
rm -rf /tmp/dune
