FROM debian:stretch
MAINTAINER "Lloyd Connellan"

# Environment variables
ENV PETSC_VERSION=3.8.3 \
    PETSC4PY_VERSION=3.8.1

USER root

# Install packages
RUN apt-get update && apt-get dist-upgrade --yes --no-install-recommends

RUN apt-get install --yes --no-install-recommends \
  ca-certificates wget tar \
  libc6-dev gcc g++ binutils make cmake pkg-config patch \
  libeigen3-dev libsuitesparse-dev \
  python3-dev python3-setuptools python3-pip python3-wheel \
  python3-pandocfilters \
  python3-numpy python3-scipy python3-matplotlib \
  python3-ufl \
  libmpich-dev \
  libhdf5-mpich-dev \
  mpich

# Install PETSc from source
RUN apt-get -y install python && \
    wget -nc --quiet https://bitbucket.org/petsc/petsc/get/v${PETSC_VERSION}.tar.gz -O petsc-${PETSC_VERSION}.tar.gz && \
    mkdir -p petsc-src && tar -xf petsc-${PETSC_VERSION}.tar.gz -C petsc-src --strip-components 1 && \
    cd petsc-src && \
    ./configure --COPTFLAGS="-O2" \
                --CXXOPTFLAGS="-O2" \
                --FOPTFLAGS="-O2" \
                --with-debugging=0 \
                --download-hypre \
                --download-suitesparse \
                --download-superlu \
                --prefix=/usr/local/petsc-32 && \
     make && \
     make install && \
     apt-get clean && \
     rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# Install PETSc4py
RUN export PETSC_DIR=/usr/local/petsc-32
RUN pip3 install --no-cache-dir mpi4py pkgconfig
RUN pip3 install --no-cache-dir https://bitbucket.org/petsc/petsc4py/downloads/petsc4py-${PETSC4PY_VERSION}.tar.gz

# Install Jupyter
RUN pip3 install jupyter

# Install DUNE
ADD install-dune.sh /root/
RUN /root/install-dune.sh

# Set up Jupyter config
ADD jupyter_config.py /etc/
RUN chmod 644 /etc/jupyter_config.py

# Set up dune as user
RUN adduser --disabled-password --home /dune --gecos "" --uid 50000 dune

ENV LD_LIBRARY_PATH "/usr/lib:/usr/local/lib"
ENV DUNE_CMAKE_FLAGS="CMAKE_BUILD_TYPE=Release BUILD_SHARED_LIBS=TRUE -DPETSC_DIR=/usr/local/petsc-32"

# Copy dune-fempy notebooks and change ownership
COPY dune-fempy.ipynb crystal.ipynb mcf.ipynb laplace-adaptive.ipynb /dune/

# Copy dune-femnv files and change ownership
COPY nvtest.ipynb problems.py schemes.py results.py norms.py matrix.py /dune/
RUN mkdir /dune/results /dune/pickle /dune/plots /dune/pickle1 /dune/pickle3
RUN chown dune:dune /dune/results /dune/pickle /dune/plots /dune/pickle1 /dune/pickle3

WORKDIR /dune

# due to the 'MPI Finalaize' error we have to let this run without checking
# for errors - make sure to go through the output of the docker build command to
# check that the notebook ran until the end
RUN chown dune:dune /dune/dune-fempy.ipynb
RUN chown dune:dune /dune/nvtest.ipynb
RUN chown dune:dune /dune/crystal.ipynb
RUN chown dune:dune /dune/mcf.ipynb
RUN chown dune:dune /dune/laplace-adaptive.ipynb
USER dune
RUN jupyter nbconvert --config=/etc/jupyter_config.py --to notebook\
    --ExecutePreprocessor.timeout=-1 --execute dune-fempy.ipynb --output dune-fempy.ipynb \
    2>&1 > /dune/build.log || echo "Execution of dune-fempy.ipynb!"
RUN jupyter nbconvert --config=/etc/jupyter_config.py --to notebook\
    --ExecutePreprocessor.timeout=-1 --execute nvtest.ipynb --output nvtest.ipynb \
    2>&1 > /dune/build.log || echo "Execution of nvtest.ipynb!"

EXPOSE 8888
VOLUME ["/dune"]
CMD ["jupyter-notebook", "--config=/etc/jupyter_config.py"]
