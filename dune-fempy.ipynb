{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Python code from the Dune-FemPy paper\n",
    "======================================\n",
    "This Jupyter notebook contains the code examples shown in the\n",
    "Dune-Python paper.\n",
    "The sectioning reflects that of the paper and, while some introductory\n",
    "comments are given, we refer to it for the details.\n",
    "\n",
    "__Note__: Some of the code cells depend on previous ones and cannot be\n",
    "executed in arbitrary order."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "import time\n",
    "import numpy\n",
    "import math\n",
    "import dune.plotting\n",
    "dune.plotting.block = False"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Finite Element Methods in Fempy\n",
    "In this section we introduce the basic components of a finite\n",
    "element method when applied to the Forchheimer problem."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "from dune.grid import structuredGrid\n",
    "grid = structuredGrid([0, 0], [1, 1], [4, 4])\n",
    "grid.plot()\n",
    "grid.hierarchicalGrid.globalRefine(1)\n",
    "grid.plot()\n",
    "grid.hierarchicalGrid.globalRefine(-1)  # revert grid refinement"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Setting up a discrete function space and some grid function"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "import dune.create as create\n",
    "space = create.space('lagrange', grid, dimrange=1, order=2)\n",
    "\n",
    "from ufl import SpatialCoordinate\n",
    "x = SpatialCoordinate(space)\n",
    "\n",
    "initial = 1/2*(x[0]**2 + x[1]**2) - 1/3*(x[0]**3 - x[1]**3) + 1"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can easily easily integrate grid function"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "from dune.fem.function import integrate\n",
    "mass = integrate(grid, initial, 5)[0]\n",
    "print(mass)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "and plot them using matplotlib or write a vtk file for postprocessing"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "from dune.fem.plotting import plotPointData as plot\n",
    "plot(initial, grid=grid)\n",
    "grid.writeVTK('initial', pointdata={'initial': initial})"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "So far we used grid functions defined globally. An important subclass of\n",
    "grid functions are discrete functions over a given discrete function space.\n",
    "The easiest way to construct such functions is to use the interpolate\n",
    "method on the discrete function space:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "u_h = space.interpolate(initial, name='u_h')\n",
    "\n",
    "u_h_n = u_h.copy(name=\"previous\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we can set up our PDE model\n",
    "\\begin{equation}\n",
    "  \\int_{\\Omega} u^{n+1} v + \\Delta t K(\\nabla u) \\nabla u^{n+1} \\cdot \\nabla v\\mathop{dx}\n",
    "          = \\int_{\\Omega} u^n v + \\Delta t f v\\ \\mathop{dx}\n",
    "\\end{equation}\n",
    "where the diffusion tensor is given by\n",
    "\\begin{equation}\n",
    "  K(\\nabla u) = \\frac{2}{1+\\sqrt{1+4\\nabla u}}\n",
    "\\end{equation}\n",
    "and we force the system by defining $f$ so that\n",
    "\\begin{equation}\n",
    "  u(x,t) = e^{-2t}\\left(\\frac{1}{2}(x_0^2 + x_1^2) - \\frac{1}{3}(x_0^3 - x_1^3)\\right) + 1\n",
    "\\end{equation}\n",
    "becomes the exact solution."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "from ufl import TestFunction, TrialFunction\n",
    "from dune.ufl import NamedConstant\n",
    "u = TrialFunction(space)\n",
    "v = TestFunction(space)\n",
    "dt = NamedConstant(space, \"dt\")    # time step\n",
    "t  = NamedConstant(space, \"t\")     # current time\n",
    "\n",
    "from ufl import dx, grad, inner, sqrt\n",
    "abs_du = sqrt(inner(grad(u), grad(u)))\n",
    "K = 2/(1 + sqrt(1 + 4*abs_du))\n",
    "a = (inner((u - u_h_n)/dt, v) + inner(K*grad(u), grad(v)))*dx\n",
    "\n",
    "from ufl import as_vector, exp\n",
    "exact = lambda t: as_vector([exp(-2*t)*(initial - 1) + 1])\n",
    "\n",
    "from ufl import replace\n",
    "b = replace(a, {u: exact(t)})"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "With the model described as a ufl form, we can construct a scheme class\n",
    "that provides the solve method which we can use to evolve the solution from\n",
    "one time step to the next:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "scheme = create.scheme(\"galerkin\", a == b, solver='cg')\n",
    "\n",
    "scheme.model.dt = 0.05\n",
    "\n",
    "def evolve(scheme, u_h, u_h_n):\n",
    "    time = 0\n",
    "    endTime = 1.0\n",
    "    while time < (endTime + 1e-6):\n",
    "        scheme.model.t = time\n",
    "        u_h_n.assign(u_h)\n",
    "        scheme.solve(target=u_h)\n",
    "        time += scheme.model.dt"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Since we have forced the system towards a given solution, we can compute\n",
    "the discretization error. First we define ufl expressions for the $L^2$\n",
    "and $H^1$ norms and will use those to compute the experimental order of\n",
    "convergence of the scheme by computing the time evolution on different grid\n",
    "levels."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "exact_end = exact(1)\n",
    "l2error_fn = inner(u_h - exact_end, u_h - exact_end)\n",
    "h1error_fn = inner(grad(u_h - exact_end), grad(u_h - exact_end))\n",
    "\n",
    "from math import log\n",
    "error = 0\n",
    "for eocLoop in range(3):\n",
    "    print('# step:', eocLoop, ', size:', grid.size(0))\n",
    "    u_h.interpolate(initial)\n",
    "    evolve(scheme, u_h, u_h_n)\n",
    "    error_old = error\n",
    "    error = sqrt( integrate(grid, l2error_fn, 5)[0] )\n",
    "    if eocLoop == 0:\n",
    "        eoc = '-'\n",
    "    else:\n",
    "        eoc = log(error/error_old)/log(0.5)\n",
    "    print('|u_h - u| =', error, ', eoc =', eoc)\n",
    "    plot(u_h)\n",
    "    grid.writeVTK('forchheimer', pointdata={'u': u_h, 'l2error':\n",
    "                  l2error_fn, 'h1error': h1error_fn}, number=eocLoop)\n",
    "    grid.hierarchicalGrid.globalRefine(1)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Alternate Solve Methods\n",
    "Here we look at different ways of solving PDEs using external\n",
    "packages and python functionality."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Implementing a simple Newton Krylov solver using the dune-fem linear solvers"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "from scipy.sparse.linalg import spsolve\n",
    "class Scheme:\n",
    "  def __init__(self, scheme):\n",
    "      self.model = scheme.model\n",
    "\n",
    "  def solve(self, target=None):\n",
    "      # create a copy of target for the residual\n",
    "      res = target.copy(name=\"residual\")\n",
    "\n",
    "      # create numpy vectors to store target and res\n",
    "      sol_coeff = target.as_numpy\n",
    "      res_coeff = res.as_numpy\n",
    "\n",
    "      n = 0\n",
    "      while True:\n",
    "          scheme(target, res)\n",
    "          absF = math.sqrt( np.dot(res_coeff,res_coeff) )\n",
    "          if absF < 1e-10:\n",
    "              break\n",
    "          matrix = scheme.assemble(target).as_numpy\n",
    "          sol_coeff -= spsolve(matrix, res_coeff)\n",
    "          n += 1\n",
    "\n",
    "scheme_cls = Scheme(scheme)\n",
    "\n",
    "grid.hierarchicalGrid.globalRefine(-2)  # revert grid refinement\n",
    "u_h.interpolate(initial)                # reset u_h to initial\n",
    "evolve(scheme_cls, u_h, u_h_n)\n",
    "plot(u_h)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Using a non linear solver from the Scipy package"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "from scipy.optimize import newton_krylov\n",
    "from scipy.sparse.linalg import LinearOperator\n",
    "\n",
    "def f(x_coeff):\n",
    "    res = u_h.copy(name=\"residual\")\n",
    "    res_coeff = res.as_numpy\n",
    "    x = space.numpyFunction(x_coeff, \"tmp\")\n",
    "    scheme(x, res)\n",
    "    return res_coeff\n",
    "\n",
    "# class for the derivative DS of S\n",
    "class Df(LinearOperator):\n",
    "    def __init__(self, x_coeff):\n",
    "        self.shape = (x_coeff.shape[0], x_coeff.shape[0])\n",
    "        self.dtype = x_coeff.dtype\n",
    "        # the following converts a given numpy array\n",
    "        # into a discrete function over the given space\n",
    "        x = space.numpyFunction(x_coeff, \"tmp\")\n",
    "        # store the assembled matrix\n",
    "        self.jac = scheme.assemble(x).as_numpy\n",
    "    # reassemble the matrix DF(u) given a DoF vector for u\n",
    "    def update(self, x_coeff, f):\n",
    "        x = space.numpyFunction(x_coeff, \"tmp\")\n",
    "        # Note: the following does produce a copy of the matrix\n",
    "        # and each call here will reproduce the full matrix\n",
    "        # structure - no reuse possible in this version\n",
    "        self.jac = scheme.assemble(x).as_numpy\n",
    "    # compute DS(u)^{-1}x for a given DoF vector x\n",
    "    def _matvec(self, x_coeff):\n",
    "        return spsolve(self.jac, x_coeff)\n",
    "\n",
    "class Scheme2:\n",
    "    def __init__(self, scheme):\n",
    "        self.scheme = scheme\n",
    "        self.model = scheme.model\n",
    "    def solve(self, target=None):\n",
    "        sol_coeff = target.as_numpy\n",
    "        # call the newton krylov solver from scipy\n",
    "        sol_coeff[:] = newton_krylov(f, sol_coeff,\n",
    "                    verbose=0, f_tol=1e-8,\n",
    "                    inner_M=Df(sol_coeff))\n",
    "\n",
    "scheme2_cls = Scheme2(scheme)\n",
    "u_h.interpolate(initial)\n",
    "evolve(scheme2_cls, u_h, u_h_n)\n",
    "plot(u_h)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Switching to a storage based on the PETSc solver package and solving the\n",
    "sysyem using the dune-fem bindings"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "space = create.space(\"lagrange\", grid, dimrange=1, order=2, storage='petsc')\n",
    "scheme = create.scheme(\"galerkin\", a == b, space=space,\n",
    "                   parameters={\"petsc.preconditioning.method\":\"sor\"})\n",
    "# first we will use the petsc solver available in the `dune-fem` package (using the sor preconditioner)\n",
    "u_h = space.interpolate(initial, name='u_h')\n",
    "u_h_n = u_h.copy(name=\"previous\")\n",
    "scheme.model.dt = 0.05\n",
    "evolve(scheme, u_h, u_h_n)\n",
    "plot(u_h)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Implementing a Newton Krylov solver using the binding provided by petsc4py"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "import petsc4py, sys\n",
    "petsc4py.init(sys.argv)\n",
    "from petsc4py import PETSc\n",
    "ksp = PETSc.KSP()\n",
    "ksp.create(PETSc.COMM_WORLD)\n",
    "# use conjugate gradients method\n",
    "ksp.setType(\"cg\")\n",
    "# and incomplete Cholesky\n",
    "ksp.getPC().setType(\"icc\")\n",
    "\n",
    "class Scheme3:\n",
    "    def __init__(self, scheme):\n",
    "        self.model = scheme.model\n",
    "    def solve(self, target=None):\n",
    "        res = target.copy(name=\"residual\")\n",
    "        sol_coeff = target.as_petsc\n",
    "        res_coeff = res.as_petsc\n",
    "        n = 0\n",
    "        while True:\n",
    "            scheme(target, res)\n",
    "            absF = math.sqrt( res_coeff.dot(res_coeff) )\n",
    "            if absF < 1e-10:\n",
    "                break\n",
    "            matrix = scheme.assemble(target).as_petsc\n",
    "            ksp.setOperators(matrix)\n",
    "            ksp.setFromOptions()\n",
    "            ksp.solve(res_coeff, res_coeff)\n",
    "            sol_coeff -= res_coeff\n",
    "            n += 1\n",
    "\n",
    "u_h.interpolate(initial)\n",
    "scheme3_cls = Scheme3(scheme)\n",
    "evolve(scheme3_cls, u_h, u_h_n)\n",
    "plot(u_h)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Using the petsc4py bindings for the non linear KSP solvers from PETSc"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "def f(snes, X, F):\n",
    "    inDF = space.petscFunction(X)\n",
    "    outDF = space.petscFunction(F)\n",
    "    scheme(inDF,outDF)\n",
    "def Df(snes, x, m, b):\n",
    "    inDF = space.petscFunction(x)\n",
    "    matrix = scheme.assemble(inDF).as_petsc\n",
    "    m.createAIJ(matrix.size, csr=matrix.getValuesCSR())\n",
    "    b.createAIJ(matrix.size, csr=matrix.getValuesCSR())\n",
    "    return PETSc.Mat.Structure.SAME_NONZERO_PATTERN\n",
    "\n",
    "class Scheme4:\n",
    "    def __init__(self, scheme):\n",
    "        self.scheme = scheme\n",
    "        self.model = scheme.model\n",
    "\n",
    "    def solve(self, target=None):\n",
    "        res = target.copy(name=\"residual\")\n",
    "        sol_coeff = target.as_petsc\n",
    "        res_coeff = res.as_petsc\n",
    "\n",
    "        snes = PETSc.SNES().create()\n",
    "        snes.setMonitor(lambda snes, i, r:print())\n",
    "        snes.setFunction(f, res_coeff)\n",
    "        matrix = self.scheme.assemble(target).as_petsc\n",
    "        snes.setJacobian(Df, matrix, matrix)\n",
    "        snes.getKSP().setType(\"cg\")\n",
    "        snes.setFromOptions()\n",
    "        snes.solve(None, sol_coeff)\n",
    "\n",
    "u_h.interpolate(initial)\n",
    "scheme4_cls = Scheme4(scheme)\n",
    "evolve(scheme4_cls, u_h, u_h_n)\n",
    "plot(u_h)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.5.2"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
